package bia.jaktenstid.remotionclient;

import android.content.SharedPreferences;

public class RecentConnectionsAdapter {
    public static final String APP_PREFERENCES = "usersettings";
    public static final String APP_PREFERNCES_CONNECTION1 = "recentconnection1";
    public static final String APP_PREFERNCES_CONNECTION2 = "recentconnection2";
    private static SharedPreferences settings;
    private static RecentConnectionsAdapter adapter;

    private RecentConnectionsAdapter(SharedPreferences settings) {
        this.settings = settings;
    }

    public static RecentConnectionsAdapter InitializeAdapter(SharedPreferences settings) {
        if (adapter == null)
            adapter = new RecentConnectionsAdapter(settings);
        return adapter;
    }

    public String[] GetRecentConnections() {
        String connection1 = settings.getString(APP_PREFERNCES_CONNECTION1, null);
        String connection2 = settings.getString(APP_PREFERNCES_CONNECTION2, null);
        return new String[]{connection1, connection2};
    }

    public void SaveLastConnection(String address) {
        SharedPreferences.Editor editor = settings.edit();
        String connection1 = settings.getString(APP_PREFERNCES_CONNECTION1, null);
        String connection2 = settings.getString(APP_PREFERNCES_CONNECTION2, null);
        if (connection1 != address & connection2 != address) {
            String temp = connection1;
            connection2 = temp;
            connection1 = address;
        }
        editor.putString(APP_PREFERNCES_CONNECTION1, connection1);
        editor.putString(APP_PREFERNCES_CONNECTION2, connection2);
        editor.apply();
    }
}
