package bia.jaktenstid.remotionclient;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.IOException;

public class TouchpadActivity extends Activity {

    private final int LANDSCAPE_ORIENTATION = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
    private final int PORTRAIT_ORIENTATION = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
    int orientation = PORTRAIT_ORIENTATION;
    //OrientationController orientationController;
    MouseMoveProvider mouseMoveProvider;
    Thread mouseMoveThread;
    Button touchpad;
    Button leftMouseButton;
    Button rightMouseButton;
    Button openKeyboardButton;
    Button shortcut1Button;
    Button shortcut2Button;
    LinearLayout keyboardLayout;
    ClientEngine client;
    EditText enterKeyboardText;
    int xHistoricalFirstFinger;
    int yHistoricalFirstFinger;
    boolean fingerDown;
    int enterTextState;
    int xFirstClick;
    int yFirstClick;
    int x;
    int y;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_touchpad);
        client = ClientEngine.clientEngine;
        context = this;
        fingerDown = true;

        //orientationController = OrientationController.InitializeController(getSharedPreferences(OrientationController.APP_PREFERENCES, Context.MODE_PRIVATE));
        //orientation = orientationController.GetCurrentOrientation();
        //ChangeOrientation();

        touchpad = (Button) findViewById(R.id.touchPadButton);
        leftMouseButton = (Button) findViewById(R.id.LeftMouseButtonTouchpad);
        rightMouseButton = (Button) findViewById(R.id.RightMouseButtonTouchpad);
        openKeyboardButton = (Button) findViewById(R.id.OpenKeyboardButton);
        enterKeyboardText = (EditText) findViewById(R.id.EnterKeyboardText);
        keyboardLayout = (LinearLayout) findViewById(R.id.KeyboardLayout);
        shortcut1Button = (Button) findViewById(R.id.shortcutButton1);
        shortcut2Button = (Button) findViewById(R.id.shortcutButton2);

        keyboardLayout.setVisibility(View.GONE);

        enterKeyboardText.setOnEditorActionListener(enterKeyboardTextOnEditorActionListener);
        enterKeyboardText.setRawInputType(InputType.TYPE_CLASS_TEXT);
        enterKeyboardText.setImeOptions(EditorInfo.IME_ACTION_GO);

        leftMouseButton.setOnTouchListener(leftMouseButtonTouchListener);

        rightMouseButton.setOnTouchListener(rightMouseButtonTouchListener);

        openKeyboardButton.setOnClickListener(openKeyboardOnTouchListener);

        touchpad.setOnTouchListener(touchpadListener);

        shortcut1Button.setOnClickListener(shortcut1ClickListener);

        shortcut2Button.setOnClickListener(shortcut2ClickListener);

        mouseMoveProvider = new MouseMoveProvider();
        mouseMoveProvider.SetContext(context);
        mouseMoveThread = new Thread(mouseMoveProvider);
        mouseMoveThread.start();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (keyboardLayout.getVisibility() == View.VISIBLE) {
                keyboardLayout.setVisibility(View.GONE);
            } else {
                Intent prev_activity = new Intent();
                prev_activity.setClass(this, MainActivity.class);
                prev_activity.setAction(MainActivity.class.getName());
                prev_activity.setFlags(
                        Intent.FLAG_ACTIVITY_CLEAR_TASK);
                Log.v("Network broadcast", "Activity starting");
                this.startActivity(prev_activity);
                client.Dispose();
                this.finish();
            }
        }
        return true;
    }

    private void ChangeOrientation() {
        if (orientation == LANDSCAPE_ORIENTATION) {
            orientation = PORTRAIT_ORIENTATION;
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            orientation = LANDSCAPE_ORIENTATION;
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
    }

    //region Listeners_init
    View.OnTouchListener leftMouseButtonTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_UP) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            client.MouseLeftButtonUp();
                        } catch (IOException e) {
                            e.printStackTrace();
                            NetworkListener.HandleNetworkIsUreachableException(context);
                        }
                    }
                }).start();
            }
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            client.MouseLeftButtonDown();
                        } catch (IOException e) {
                            e.printStackTrace();
                            NetworkListener.HandleNetworkIsUreachableException(context);
                        }
                    }
                }).start();
            }

            return false;
        }
    };

    View.OnTouchListener rightMouseButtonTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_UP) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            client.MouseRightButtonUp();
                        } catch (IOException e) {
                            e.printStackTrace();
                            NetworkListener.HandleNetworkIsUreachableException(context);
                        }
                    }
                }).start();
            }
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            client.MouseRightButtonDown();
                        } catch (IOException e) {
                            e.printStackTrace();
                            NetworkListener.HandleNetworkIsUreachableException(context);
                        }
                    }
                }).start();
            }
            return false;
        }
    };

    View.OnClickListener openKeyboardOnTouchListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (enterTextState == 0) {
                keyboardLayout.setVisibility(View.VISIBLE);
                enterTextState = 1;
            } else {
                enterTextState = 0;
                keyboardLayout.setVisibility(View.GONE);
            }
        }
    };

    View.OnTouchListener touchpadListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            //if (counter == 0) {
            if (fingerDown) {
                xHistoricalFirstFinger = (int) event.getX();
                yHistoricalFirstFinger = (int) event.getY();
                xFirstClick = xHistoricalFirstFinger;
                yFirstClick = yHistoricalFirstFinger;
                //counter++;
                fingerDown = false;
            }
            x = (int) event.getX();
            y = (int) event.getY();
            Log.v("Touchpad PORTRAIT", client.remoteAdress.toString());

            mouseMoveProvider.nextX = x - xHistoricalFirstFinger;
            mouseMoveProvider.nextY = y - yHistoricalFirstFinger;
            mouseMoveProvider.Ready = true;
            xHistoricalFirstFinger = x;
            yHistoricalFirstFinger = y;

            if (event.getAction() == MotionEvent.ACTION_UP) {
                if (x - xFirstClick == 0 & y - yFirstClick == 0) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                client.MouseLeftButtonClick();
                            } catch (IOException e) {
                                e.printStackTrace();
                                NetworkListener.HandleNetworkIsUreachableException(context);
                            }
                        }
                    }).start();
                }
                //counter = 0;
                fingerDown = true;
                return true;
            }
            return true;
        }
    };

    TextView.OnEditorActionListener enterKeyboardTextOnEditorActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_GO) {
                final String text = enterKeyboardText.getText().toString();
                enterKeyboardText.getText().clear();

                new Thread(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            client.SendCustomMessage(text);
                            client.EnterPress();
                        } catch (IOException e) {
                            e.printStackTrace();
                            NetworkListener.HandleNetworkIsUreachableException(context);
                        }
                    }
                }).start();
            }
            return true;
        }
    };

    View.OnClickListener shortcut1ClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(shortcut2Button.isPressed())
            {
                ChangeOrientation();
                return;
            }
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        client.SettingsCommand("1");
                    } catch (IOException e) {
                        e.printStackTrace();
                        NetworkListener.HandleNetworkIsUreachableException(context);
                    }
                }
            }).start();
        }
    };

    View.OnClickListener shortcut2ClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(shortcut1Button.isPressed())
            {
                ChangeOrientation();
                return;
            }
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        client.SettingsCommand("2");
                    } catch (IOException e) {
                        e.printStackTrace();
                        NetworkListener.HandleNetworkIsUreachableException(context);
                    }
                }
            }).start();
        }
    };


    //endregion
}
