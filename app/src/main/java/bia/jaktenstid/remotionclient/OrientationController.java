package bia.jaktenstid.remotionclient;

import android.content.SharedPreferences;


public class OrientationController {
    public static final String APP_PREFERENCES = "usersettings";
    public static final String APP_PREFERNCES_LAST_ORIENTATION = "lastorientation";
    public static final String LANDSCAPE_ORIENTATION = "landscape";
    public static final String PORTRAIT_ORIENTATION = "portrait";
    private static SharedPreferences settings;
    private static OrientationController saver;

    private OrientationController(SharedPreferences settings) {
        this.settings = settings;
    }

    public static OrientationController InitializeController(SharedPreferences settings) {
        if (saver == null)
            saver = new OrientationController(settings);
        return saver;
    }

    public void ChangeOrientation() {
        String lastOrientation = settings.getString(APP_PREFERNCES_LAST_ORIENTATION, LANDSCAPE_ORIENTATION);
        switch (lastOrientation) {
            case (LANDSCAPE_ORIENTATION): {
                SharedPreferences.Editor editor = settings.edit();
                editor.putString(APP_PREFERNCES_LAST_ORIENTATION, PORTRAIT_ORIENTATION);
                editor.apply();
            }
            break;
            case (PORTRAIT_ORIENTATION): {
                SharedPreferences.Editor editor = settings.edit();
                editor.putString(APP_PREFERNCES_LAST_ORIENTATION, LANDSCAPE_ORIENTATION);
                editor.apply();
            }
            break;
        }
    }

    public String GetCurrentOrientation()
    {
        return settings.getString(APP_PREFERNCES_LAST_ORIENTATION, LANDSCAPE_ORIENTATION);
    }
}

