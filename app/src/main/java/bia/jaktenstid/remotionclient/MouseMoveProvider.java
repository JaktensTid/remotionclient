package bia.jaktenstid.remotionclient;

import android.content.Context;
import android.util.Log;

import java.io.IOException;

public class MouseMoveProvider implements Runnable {
    public boolean Ready;
    public int nextX;
    public int nextY;
    private ClientEngine client;
    private Context context;

    @Override
    public void run() {
        client = ClientEngine.clientEngine;
        while (true) {
            if (Ready) {
                Ready = false;
                try {
                    client.MouseMove(nextX, nextY);
                } catch (IOException e) {
                    e.printStackTrace();
                    NetworkListener.HandleNetworkIsUreachableException(context);
                }
            }
        }
    }

    public void SetContext(Context context) {
        this.context = context;
    }
}
