package bia.jaktenstid.remotionclient;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.util.Log;


public class NetworkListener {
    private static void GoToMainActivity(final Context context) {
        Handler handler = new Handler(context.getMainLooper());
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                String alert_message = context.getString(R.string.network_fail_message);
                String alert_title = context.getString(R.string.network_fail);
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage(alert_message)
                        .setTitle(alert_title)
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent intent = new Intent();
                                intent.setClass(context, MainActivity.class);
                                intent.setAction(MainActivity.class.getName());
                                intent.setFlags(
                                        Intent.FLAG_ACTIVITY_NEW_TASK
                                                | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                                Log.v("Network broadcast", "Activity starting");
                                context.startActivity(intent);
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        };
        handler.post(runnable);
    }

    public static void HandleNetworkIsUreachableException(final Context context) {
        if (!isOnline(context)) {
            GoToMainActivity(context);
        }
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }
}
