package bia.jaktenstid.remotionclient;

import android.util.Log;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;


public class ClientEngine {
    final String ip;
    DatagramSocket socket;
    InetAddress remoteAdress;
    DatagramPacket sendingPacket;
    final String LMBCLICK = "10";
    final String RMBCLICK = "11";
    final String LMBDOWN = "14";
    final String LMBUP = "15";
    final String RMBDOWN = "16";
    final String RMBUP = "17";
    final String ENTER = "20";
    final String SYSTEMCOMMAND = "s";
    final String MOUSECOMMAND = "m";
    final String TEXTMESSAGECOMMAND = "t";
    final String MOUSEMOVECOMMAND = "r";
    final String SETTINGSCOMMAND = "e";
    public final int PORT = 48856;

    public static ClientEngine clientEngine;

    private ClientEngine(String ip) throws SocketException, UnknownHostException {
        this.ip = ip;
        socket = new DatagramSocket();
        remoteAdress = InetAddress.getByName(ip);
    }

    public static void CreateClientSocket(String ip) {
        if (clientEngine == null) {
            try {
                clientEngine = new ClientEngine(ip);
            } catch (SocketException e) {
                e.printStackTrace();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
        }
    }

    public void MouseLeftButtonClick() throws IOException {
        byte[] sendDatagram = (MOUSECOMMAND + LMBCLICK).getBytes();
        sendingPacket = new DatagramPacket(sendDatagram, sendDatagram.length, remoteAdress, PORT);
        socket.send(sendingPacket);
    }

    public void MouseLeftButtonUp() throws IOException {
        byte[] sendDatagram = (MOUSECOMMAND + LMBUP).getBytes();
        sendingPacket = new DatagramPacket(sendDatagram, sendDatagram.length, remoteAdress, PORT);
        socket.send(sendingPacket);
    }

    public void MouseLeftButtonDown() throws IOException {
        byte[] sendDatagram = (MOUSECOMMAND + LMBDOWN).getBytes();
        sendingPacket = new DatagramPacket(sendDatagram, sendDatagram.length, remoteAdress, PORT);
        socket.send(sendingPacket);
    }

    public void MouseRightButtonUp() throws IOException {
        byte[] sendDatagram = (MOUSECOMMAND + RMBUP).getBytes();
        sendingPacket = new DatagramPacket(sendDatagram, sendDatagram.length, remoteAdress, PORT);
        socket.send(sendingPacket);
    }

    public void MouseRightButtonDown() throws IOException {
        byte[] sendDatagram = (MOUSECOMMAND + RMBDOWN).getBytes();
        sendingPacket = new DatagramPacket(sendDatagram, sendDatagram.length, remoteAdress, PORT);
        socket.send(sendingPacket);
    }

    public void MouseMove(int X, int Y) throws IOException {
        String sending = MOUSEMOVECOMMAND + X + ":" + Y;
        byte[] sendDatagram = sending.getBytes();
        sendingPacket = new DatagramPacket(sendDatagram, sendDatagram.length, remoteAdress, PORT);
        socket.send(sendingPacket);
        Log.v("Touchpad sended: ", X + " - " + Y + " to " + socket.getInetAddress());
    }

    public void SendCustomMessage(String message) throws IOException {
        String sending = TEXTMESSAGECOMMAND + message;
        byte[] sendDatagram = sending.getBytes("UTF-8");
        sendingPacket = new DatagramPacket(sendDatagram, sendDatagram.length, remoteAdress, PORT);
        socket.send(sendingPacket);
    }

    public void EnterPress() throws IOException {
        byte[] sendDatagram = (SYSTEMCOMMAND + ENTER).getBytes();
        sendingPacket = new DatagramPacket(sendDatagram, sendDatagram.length, remoteAdress, PORT);
        socket.send(sendingPacket);
    }

    public void SettingsCommand(String modificator) throws IOException {
        byte[] sendDatagram = (SETTINGSCOMMAND + modificator).getBytes();
        sendingPacket = new DatagramPacket(sendDatagram, sendDatagram.length, remoteAdress, PORT);
        socket.send(sendingPacket);
    }

    public void Dispose() {
        socket.close();
        remoteAdress = null;
        clientEngine = null;
    }
}
