package bia.jaktenstid.remotionclient;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

public class MainActivity extends FragmentActivity implements View.OnClickListener {

    EditText ipText;
    Button connectButton;
    Pattern pattern = Pattern.compile("^(?:[0-9]{1,3}\\.){3}[0-9]{1,3}$");
    Vibrator vibrator;
    String[] recentAddresses;
    RecentConnectionsAdapter recentConnectionsAdapter;
    //ListView listView;
    //TextView recentConnectionsTextView;
    TextView ipAddressTextView;
    //ArrayAdapter<String> adapter;
    NetworkListener networkListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        setContentView(R.layout.activity_main);
        //listView = (ListView) findViewById(R.id.recentConnectionsListView);
        //recentConnectionsTextView = (TextView) findViewById(R.id.recentConnectionsTextView);
        recentConnectionsAdapter = RecentConnectionsAdapter.InitializeAdapter(getSharedPreferences(RecentConnectionsAdapter.APP_PREFERENCES, Context.MODE_PRIVATE));
        recentAddresses = recentConnectionsAdapter.GetRecentConnections();
        ipText = (EditText) findViewById(R.id.ipEditText);
        ipAddressTextView = (TextView) findViewById(R.id.ipAddressTextView);
        connectButton = (Button) findViewById(R.id.connectButton);
        connectButton.setOnClickListener(this);
        networkListener = new NetworkListener();

        if (recentAddresses[0] == null) {
            //listView.setVisibility(View.GONE);
            //recentConnectionsTextView.setVisibility(View.GONE);
        } else {
            ipText.setText(recentAddresses[0]);
            //ArrayList<String> nextArr = new ArrayList<>();
            //nextArr.add(recentAddresses[0]);
            //if (recentAddresses[1] != null) nextArr.add(recentAddresses[1]);
            //adapter = new ArrayAdapter<>(this,
            //        android.R.layout.simple_list_item_1, nextArr.toArray(new String[]{}));
            //listView.setAdapter(adapter);
        }
        //registerReceiver(networkListener, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        //listView.setOnItemClickListener(listViewOnItemClickListener);
        NetworkListener.isOnline(this);
    }

    @Override
    public void onClick(View v) {
        boolean isValidInput = CheckForConnection();

        if (isValidInput) {
            String address = ipText.getText().toString();
            Intent intent = new Intent(this, TouchpadActivity.class);
            ClientEngine.CreateClientSocket(address);
            recentConnectionsAdapter.SaveLastConnection(address);
            startActivity(intent);
        }
    }

    public boolean CheckForConnection() {
        if (ipText.getText().toString() == "") {
            vibrator.vibrate(200);
            return false;
        }
        if (!pattern.matcher(ipText.getText().toString()).matches()) {
            vibrator.vibrate(200);
            new ConnectionErrorNotificator(ipAddressTextView, getResources().getString(R.string.invalid_ip_address), getResources().getString(R.string.ip_address)).execute();
            return false;
        }
        if (!NetworkListener.isOnline(this)) {
            vibrator.vibrate(200);
            new ConnectionErrorNotificator(ipAddressTextView, getResources().getString(R.string.network_fail), getResources().getString(R.string.ip_address)).execute();
            return false;
        }
        return true;
    }

    /*AdapterView.OnItemClickListener listViewOnItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            String s = (String) listView.getItemAtPosition(position);
            ipText.setText(s);
        }
    };*/


    private class ConnectionErrorNotificator extends AsyncTask<Void, Void, Void> {
        TextView textView;
        String begin;
        String end;

        public ConnectionErrorNotificator(TextView textView, String begin, String end) {
            this.textView = textView;
            this.begin = begin;
            this.end = end;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            textView.setText(begin);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            textView.setText(end);
        }
    }
}
